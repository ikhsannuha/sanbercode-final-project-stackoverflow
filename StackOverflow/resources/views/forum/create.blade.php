@extends('layouts.app')
@section('title','Buat Pertanyaan')
@section('content')
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({selector:'textarea'});</script>
<div class="container">
     <div class="jumbotron" id="tc_jumbotron">
        <div class="col-md-8 offset-md-2">
          <div class="text-center"><h3 style="color: #000;">Buat Pertanyaan</h3></div><hr style="background: #000"> 
        </div>
      <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card"> 
                <div class="card-body">
                   <form action="{{route('forum.store')}}" method="post"  enctype="multipart/form-data">
                      {{csrf_field()}}
                    <div class="form-group">
                      <input type="text" id="tc_input" class="form-control" name="title" placeholder="Title"> 
                    </div>
                    
                        
                <div class="bg">
                   <div class="form-group">
                        <textarea type="text" class="form-control" name="description" placeholder="description"> </textarea>
                      </div>            
                </div>
                <div class="form-group">
                    <select name="tags" id="tc_input" class="form-control" id="">
                        @foreach($tags as $tag)
                         <option value="{{$tag->id}}">{{$tag->name}}</option>
                         @endforeach
                    </select>
                </div>
               
              <div class="bg">
                 <div class="form-group">
                     <input type="file" class="form-control" name="image" placeholder="image" style="background-color: #f5f8fa;"> 
                  </div>
              </div> 
             <br> 
            
             <button type="submit" class="btn btn-success btn-block">Submit</button>
               </form>
              </div>
              </div>
           <br>

          
          <div class="alert alert-info alert-dismissible fade show" role="alert">
          @forelse($forums as $forum)
          <b>Pertanyaan Terakhir Anda:</b><br>

           <a href="#" style="color: #444"><h5 style="margin-top: 4px;"><i class="fa fa-newspaper-o"></i> {{$forum->title}}</h5></a> 
           @empty
        <strong> Pertanyaan baru akan muncul disini.</strong><br> 
          @endforelse
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
            </div>
        </div>
    </div>
</div>
@endsection
 