<?php


Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/forum','ForumController');

Route::get('/forum/read/{slug}','ForumController@show')->name('forumslug');

Route::post('/comment/addComment/{forum}','CommentController@addComment')->name('addComment');

Route::post('/reply/addReply/{forum}','ReplyController@addReply')->name('addReply');
