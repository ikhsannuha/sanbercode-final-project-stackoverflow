<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;
use App\User;
class forum extends Model
{
    public function tags()
    {
    	return $this->belongsToMany('App\Tag');

    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function comments()
    {
    	return $this->morphMany('App\Comment','commentable');
    }
    public function replies()
    {
        return $this->morphMany('App\Reply','commentable');
    }
}
