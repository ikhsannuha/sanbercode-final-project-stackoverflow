<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    public function commentable()
    {
    	return $this->morphTo();
    }
}
