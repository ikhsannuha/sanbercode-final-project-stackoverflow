<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Forum;
use App\Reply;
use Auth;
class ReplyController extends Controller
{
    public function addReply(Request $request, Forum $forum)
    {
    	$request->validate([
    		'konten' => 'required|min:3',
    	]);
    	$reply = New Reply;
    	$reply->user_id = Auth::user()->id;
    	$reply->konten = $request->konten;

    	$forum->replies()->save($reply);

    	return back()->withInfo('Balasan Terkirim!');

    }
}
